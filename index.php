
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Nicholas Metz">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="scripts.js"></script>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-brand">
          Nicholas Metz, Assignment #13
        </div>
        
      </div>
    </nav>
    <div class="row">
        <fieldset class="col-sm-1 col-sm-offset-6">
            <legend>Radio buttons</legend>
            <div class="form-check">
                <label class="form-check-label">
                <input type="radio" class="form-check-input" name="data" id="books" value="books" checked>
                Books
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                <input type="radio" class="form-check-input" name="data" id="donuts" value="donuts">
                Donuts
                </label>
            </div>
        </fieldset>
        
    </div>
    <div class="row">
        <div class="col-sm-4 col-sm-offset-5">
            <button type="buttton" class="btn btn-primary">Raw XML</button>
            <button type="buttton" class="btn btn-success">Raw JSON</button>
            <button type="buttton" class="btn btn-info">Displayed XML</button>
            <button type="buttton" class="btn btn-warning">Displayed JSON</button>
        </div>        
    </div>
    <div id="drop" class="container">
        <div class="row">
            <?php
            if (file_exists('books.xml')) {
                $xml = simplexml_load_file('books.xml');
                echo ("<pre>");
                print_r($xml);
                echo ("</pre>");

            } else {
                exit('Failed to open books.xml.');
            }
            ?>
        </div>
    </div>
    
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  </body>
</html>